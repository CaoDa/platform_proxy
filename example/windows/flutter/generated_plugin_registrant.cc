//
//  Generated file. Do not edit.
//

#include "generated_plugin_registrant.h"

#include <platform_proxy/platform_proxy_plugin.h>

void RegisterPlugins(flutter::PluginRegistry* registry) {
  PlatformProxyPluginRegisterWithRegistrar(
      registry->GetRegistrarForPlugin("PlatformProxyPlugin"));
}
